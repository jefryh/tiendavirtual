package pe.edu.ucsp.cs.is2.repository;

import java.util.List;

import pe.edu.ucsp.cs.is2.domain.Administer;
import pe.edu.ucsp.cs.is2.domain.Category;
import pe.edu.ucsp.cs.is2.domain.Promotion;
import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.User;

public interface AdministerRepository {
	
	Administer save(Administer administer);
	Administer findByName(String name);
	Administer findById(int id);
	void updateName(int id, String name);
	void updateLastName(int id, String lastname);
	void updateDate(int id, String date);
	//void updatePassword(int id, String password);
	void updateUsername(int id, String username);
	void updateEmail(int id, String email);
	List<Administer> findAll();
	Product insertProduct(Product product);
	Category insertCategory(Category category, String name);
	Promotion insertPromotion(Promotion promotion, String name_product);
	User findUser(int id);
	void updatePromotion(int id, float dis);
	void updatePromotion(int id, String end);
	int updateStock(String product_name, int stock);
	long getnumberProductCategory(int id);
	List<Product> getBestScoreProductCategory(int id);
	List<Product> getTopProducts();
	List<Product> getLessProducts();
	List<Product> getProductsPrice(String maner);
}
