package pe.edu.ucsp.cs.is2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.ucsp.cs.is2.domain.Account;
import pe.edu.ucsp.cs.is2.domain.User;
import pe.edu.ucsp.cs.is2.repository.AccountRepository;
import pe.edu.ucsp.cs.is2.repository.ProductRepository;
import pe.edu.ucsp.cs.is2.repository.UserRepository;

@RestController
@RequestMapping("/")
public class AccountController {
	@Autowired
	UserRepository userrep;
	AccountRepository accountRepository;
	ProductRepository productRepository;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	List<Account> listAll() {
		return accountRepository.findAll();
	}
	
	@RequestMapping(value = "filter", method=RequestMethod.GET)
    public @ResponseBody Account findByEmail(@RequestParam(value="email", required=true) String email) {
        return accountRepository.findByEmail(email);
    }
	@RequestMapping("/login.html")
	public @ResponseBody User findByUserName(@RequestParam(value="usuario", required=true) String username)	{
		return userrep.findByUserName(username);
	}
	

}
