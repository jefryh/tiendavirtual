package pe.edu.ucsp.cs.is2.domain;
import java.util.ArrayList;
import java.util.List;

public class Product {
	private int id;
	private String name;
	private float priece;
	private String description;
	private boolean type_Product;
	private int score; 
	private int category_id;
	private boolean purchase_activated;
	private int stock; 
	ArrayList promotions;
	int num_downloads;
	
	public Product (int id,String name,float priece,String description,int category_id,int stock){
		ArrayList promotions = new ArrayList();
		this.id=id;
		this.name = name;
		this.priece=priece;
		this.description=description;
		this.category_id=category_id;
		this.stock=stock;
		this.promotions = promotions;
		this.num_downloads=0;
	}
	
	public int getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public float getPriece(){
		return priece;
	}
	public String getDescription(){
		return description;
	}
	public boolean getType_Product(){
		return type_Product;
	}
	public int GetScore(){
		return score;
	}
	public int getCategory_id(){
		return category_id;
	};
	public boolean getPurchase_activated(){
		return purchase_activated;
	}
	public int getStock(){
		return stock;
	}
	
	public ArrayList getPromotions(){
		return promotions;
	}
	public void setId(int id){
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setPriece(float priece){
		this.priece=priece;
	}
	public void setDescription(String description){
		this.description=description;
	}
	public void setType_Product(boolean type_Product){
		this.type_Product=type_Product;
	}
	public void setScore(int score){
		this.score=score;
	}
	public void setCategory_id(int category_id){
		this.category_id=category_id;
	};
	public void setPurchase_activated(boolean purchase_activated){
		this.purchase_activated=purchase_activated;
	}
	public void setStock(int stock){
		this.stock=stock;
	}
	
	public void increase(){
		this.num_downloads+=1;
	}
};