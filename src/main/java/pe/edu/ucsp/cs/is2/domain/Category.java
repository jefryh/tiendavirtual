package pe.edu.ucsp.cs.is2.domain;
import java.util.ArrayList;

public class Category {
	private int id;
	private String name;
	ArrayList sub_category;
	
	public Category(int id, String name) {
		ArrayList<Category> sub_category = new ArrayList();
		this.id = id;
		this.name = name;
		this.sub_category = sub_category;
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList getSub_Category() {
		return sub_category;
	}
	
}
