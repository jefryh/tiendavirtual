package pe.edu.ucsp.cs.is2.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.Promotion;
import pe.edu.ucsp.cs.is2.domain.User;
import pe.edu.ucsp.cs.is2.repository.PromotionRepository;

@Repository
public class PromotionRepositoryImpl implements PromotionRepository {

	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;
	
	@Override
	public Promotion save(Promotion promo, String name_product) {
		Query query = Query.query(Criteria.where("name").is(name_product));
		Product p = mongoTemplate.findOne(query, Product.class);
		p.getPromotions().add(promo.getId());
		mongoTemplate.save(promo);
		mongoTemplate.updateFirst(Query.query(Criteria.where("name").is(name_product)),Update.update("promotions",p.getPromotions()), Product.class);
		return promo;
	}
	
	public Promotion findByName(String promotion) {
		Query query = Query.query(Criteria.where("promotion").is(promotion));
		return mongoTemplate.findOne(query, Promotion.class);	
	}
	
	public Promotion findById(int id) {
		Query query = Query.query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Promotion.class);	
	}
}
