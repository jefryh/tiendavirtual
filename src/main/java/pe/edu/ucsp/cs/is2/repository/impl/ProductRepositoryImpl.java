package pe.edu.ucsp.cs.is2.repository.impl;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.Promotion;
import pe.edu.ucsp.cs.is2.repository.ProductRepository;

@Repository
public class ProductRepositoryImpl implements ProductRepository{
	
	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;
	
	@Override
	public Product save(Product product){
		mongoTemplate.save(product);
		return product;
	}
	@Override
	public Product findByName(String name) {
		Query query = Query.query(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, Product.class);	
	}
	
	@Override
	public Product findById(int id) {
		Query query = Query.query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Product.class);	
	}

	@Override
	public List<Product> scoreRank() {
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC,"score"));
		return mongoTemplate.find(query, Product.class);
	}
	
	@Override
	public List<Product> findAll() {
		return mongoTemplate.findAll(Product.class);
	}
	
	public ArrayList<Promotion> findPromotion(String name_product){
		Query query = Query.query(Criteria.where("name").is(name_product));
		Product prod = mongoTemplate.findOne(query, Product.class);
		return prod.getPromotions();
	}
}
