package pe.edu.ucsp.cs.is2.repository;

import java.util.ArrayList;
import java.util.List;

import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.Promotion;


public interface ProductRepository {
	
	Product save(Product product);
	Product findByName(String name);
	Product findById(int id);
	List<Product> scoreRank();
	List<Product> findAll();
	ArrayList<Promotion> findPromotion(String name_product);
	
	
}
