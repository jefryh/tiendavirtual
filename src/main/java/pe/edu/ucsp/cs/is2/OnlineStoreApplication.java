package pe.edu.ucsp.cs.is2;

import java.net.UnknownHostException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoTypeMapper;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import pe.edu.ucsp.cs.is2.domain.Account;
import pe.edu.ucsp.cs.is2.domain.Category;
import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.Promotion;
import pe.edu.ucsp.cs.is2.domain.Administer;
import pe.edu.ucsp.cs.is2.domain.User;
import pe.edu.ucsp.cs.is2.repository.AccountRepository;
import pe.edu.ucsp.cs.is2.repository.CategoryRepository;
import pe.edu.ucsp.cs.is2.repository.ProductRepository;
import pe.edu.ucsp.cs.is2.repository.PromotionRepository;
import pe.edu.ucsp.cs.is2.repository.AdministerRepository;
import pe.edu.ucsp.cs.is2.repository.UserRepository;

//import pe.edu.ucsp.edu.cs.is2.business.Logic;


import com.mongodb.Mongo;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "pe.edu.ucsp.cs.is2.repository", "pe.edu.ucsp.cs.is2.business" })
public class OnlineStoreApplication implements CommandLineRunner {

	@Value("${mongodb.database}")
	private String database;

	@Value("${mongodb.hostname}")
	private String hostname;

	@Value("${mongodb.port}")
	private String port;

	@Value("${mongodb.username}")
	private String username;

	@Value("${mongodb.password}")
	private String password;

	// Placeholder Configuration
	@Bean
	public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
		PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		ppc.setLocation(new ClassPathResource("/persistence.properties"));
		return ppc;
	}

	// Mongo Configuration
	@SuppressWarnings("deprecation")
	@Bean
	public MongoDbFactory mongoDbFactory() throws UnknownHostException {
		if (username == null || username.isEmpty()) {
			return new SimpleMongoDbFactory(new Mongo(hostname), database);
		} else {
			UserCredentials credentials = new UserCredentials(username,
					password);
			return new SimpleMongoDbFactory(new Mongo(hostname), database,
					credentials);
		}
	}

	@Bean
	public MongoTemplate mongoTemplate() throws UnknownHostException {
		MongoTemplate template = new MongoTemplate(mongoDbFactory(),
				mongoConverter());
		return template;
	}

	@Bean
	public MongoTypeMapper mongoTypeMapper() {
		return new DefaultMongoTypeMapper(null);
	}

	@Bean
	public MongoMappingContext mongoMappingContext() {
		return new MongoMappingContext();
	}

	@SuppressWarnings("deprecation")
	@Bean
	public MappingMongoConverter mongoConverter() throws UnknownHostException {
		MappingMongoConverter converter = new MappingMongoConverter(mongoDbFactory(), mongoMappingContext());
		converter.setTypeMapper(mongoTypeMapper());
		return converter;
	}

	@Autowired
	private AccountRepository repository;
	@Autowired
	private ProductRepository productrep;
	@Autowired
	private PromotionRepository promorep;
	@Autowired
	private CategoryRepository categrep;
	@Autowired
	private AdministerRepository adminrep;
	@Autowired
	private UserRepository userrep;
//	@Autowired
//	private Logic logic;
	@Override
	public void run(String... args) throws Exception {
		// save a couple of customers
		//repository.save(new Account("alice@mail.com", "Smith"));
		//repository.save(new Account("bob@mail.com", "Smith"));
		productrep.save(new Product(1,"pesas",120,"Producto para hacer ejercicio",1,10));
		promorep.save(new Promotion(1, "12/12/2012", "01/01/2013", (float) 0.1), "pesas");
		categrep.save(new Category(1,"deportes"),"");
		categrep.save(new Category(2,"futbol"),"deportes");
		categrep.save(new Category(3,"voley"),"deportes");
		userrep.save(new User(1,"Juan","Perez","JuanP","1234","juan.p@gmail.com","12/12/90"));
		userrep.updatePassword(1,"4321");
		adminrep.save(new Administer(1,"Jeffrey","Aguirre","Jefry.H","1234","j.jefry@gmail.com","26/01/91"));
		adminrep.updateStock("pesas", 20);
		adminrep.updateName(1, "jj");
		userrep.updateCash(1,30.0);
		
//		logic.verifyBuy(1, 1);	
		
		
		// fetch all customers
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		for (Account customer : repository.findAll()) {
			System.out.println(customer);
		}
		System.out.println();

		// fetch an individual customer
		System.out
				.println("Customer found with findByFirstName('alice@mail.com'):");
		System.out.println("--------------------------------");
		System.out.println(repository.findByEmail("AliceMongo(hostname)"));

	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(OnlineStoreApplication.class, args);
	}
}
