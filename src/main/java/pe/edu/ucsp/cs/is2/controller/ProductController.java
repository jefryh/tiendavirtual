package pe.edu.ucsp.cs.is2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.repository.ProductRepository;
import pe.edu.ucsp.cs.is2.repository.UserRepository;

@RestController
@RequestMapping("/product")

public class ProductController {
	@Autowired
	UserRepository userrep;
	ProductRepository productRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	
	List<Product> listAllProduct(){
		return productRepository.findAll();
	}
	
	@RequestMapping(value = "filter", method=RequestMethod.GET)
    public @ResponseBody Product findByName(@RequestParam(value="name", required=true) String name) {
        return productRepository.findByName(name);
	}
}
