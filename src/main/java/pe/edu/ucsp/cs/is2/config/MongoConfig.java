package pe.edu.ucsp.cs.is2.config;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoTypeMapper;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import com.mongodb.MongoClient;


@Configuration
class MongoConfig {
	@Value("${mongodb.database}")
	private String database;

	@Value("${mongodb.hostname}")
	private String hostname;

	@Value("${mongodb.port}")
	private String port;

	@Value("${mongodb.username}")
	private String username;

	@Value("${mongodb.password}")
	private String password;

    @Bean
    public MongoDbFactory mongoDbFactory() throws UnknownHostException {
		if (username == null || username.isEmpty()) {
			return new SimpleMongoDbFactory(new MongoClient(hostname), database);
		} else {
			UserCredentials credentials = new UserCredentials(username,
					password);
			return new SimpleMongoDbFactory(new MongoClient(hostname),
					database, credentials);
		}
    }

    @Bean
    public MongoTemplate mongoTemplate() throws UnknownHostException {
        MongoTemplate template = new MongoTemplate(mongoDbFactory(), mongoConverter());
        return template;
    }

    @Bean
    public MongoTypeMapper mongoTypeMapper() {
        return new DefaultMongoTypeMapper(null);
    }

    @Bean
    public MongoMappingContext mongoMappingContext() {
        return new MongoMappingContext();
    }

    @Bean
    public MappingMongoConverter mongoConverter() throws UnknownHostException {
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext());
        converter.setTypeMapper(mongoTypeMapper());
        return converter;
    }
}
