package pe.edu.ucsp.cs.is2.domain;

//import org.codehaus.jackson.annotate.JsonIgnore;

public class Account {

	private String id;

	private String email;
	
//	@JsonIgnore
	private String password;

	private String role = "ROLE_USER";

    protected Account() {

	}
	
	public Account(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public Account(String email, String password, String role) {
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
