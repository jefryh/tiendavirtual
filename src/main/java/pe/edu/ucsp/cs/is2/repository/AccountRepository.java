package pe.edu.ucsp.cs.is2.repository;

import java.util.List;

import pe.edu.ucsp.cs.is2.domain.Account;

public interface AccountRepository {
	
	Account save(Account account);
	
	Account findByEmail(String email);

	List<Account> findAll();
	
	void remove(Account account);
	
}
