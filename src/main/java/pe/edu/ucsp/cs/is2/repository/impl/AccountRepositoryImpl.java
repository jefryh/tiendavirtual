package pe.edu.ucsp.cs.is2.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.cs.is2.domain.Account;
import pe.edu.ucsp.cs.is2.repository.AccountRepository;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;

	@Override
	public Account save(Account account) {
		mongoTemplate.save(account);
		return account;
	}

	@Override
	public void remove(Account account) {
		mongoTemplate.remove(account);
	}

	@Override
	public Account findByEmail(String email) {
		Query query = Query.query(Criteria.where("email").is(email));
		return mongoTemplate.findOne(query, Account.class);
	}

	@Override
	public List<Account> findAll() {
		return mongoTemplate.findAll(Account.class);
	}

}
