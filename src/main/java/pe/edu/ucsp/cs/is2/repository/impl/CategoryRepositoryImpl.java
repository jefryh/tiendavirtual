package pe.edu.ucsp.cs.is2.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.cs.is2.domain.Category;
import pe.edu.ucsp.cs.is2.repository.CategoryRepository;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;
	
	@Override
	public Category save(Category category, String name) {
		if(name == ""){
			mongoTemplate.save(category);
			return category;
		}
		else{
			Category c = findByName(name);
			c.getSub_Category().add(category.getId());
			mongoTemplate.save(category);
			mongoTemplate.updateFirst(Query.query(Criteria.where("name").is(name)),Update.update("sub_category",c.getSub_Category()), Category.class);
			return category;
		}
	}
	
	@Override
	public Category findByName(String name) {
		Query query = Query.query(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, Category.class);	
	}
	
	@Override
	public Category findById(int id) {
		Query query = Query.query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Category.class);	
	}

	@Override
	public List<Category> findAll() {
		return mongoTemplate.findAll(Category.class);
	}
	
}
