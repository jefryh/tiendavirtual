package pe.edu.ucsp.cs.is2.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;


import pe.edu.ucsp.cs.is2.domain.Category;
import pe.edu.ucsp.cs.is2.domain.Administer;
import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.User;
import pe.edu.ucsp.cs.is2.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements  UserRepository{
	
	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;
	
	@Autowired(required = true)
	MongoOperations mongoOperations;
		
	@Override
	public User findByName(String name) {
		Query query = Query.query(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, User.class);	
	}
	
	@Override
	public User findById(int id) {
		Query query = Query.query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, User.class);	
	}
	
	@Override
	public void updateName(int id, String name){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("name");
 
		Update update = new Update();
		update.set("name", name);
 
		mongoOperations.updateFirst(query, update, User.class);
	}
	
	@Override
	public void updateLastName(int id, String lastname){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("last_name");
 
		Update update = new Update();
		update.set("last_name", lastname);
 
		mongoOperations.updateFirst(query, update, User.class);
	}
	
	@Override
	public void updateDate(int id, String date){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("birthdate");
 
		Update update = new Update();
		update.set("birthdate", date);
 
		mongoOperations.updateFirst(query, update, User.class);
	}
	
	/*public void updatePassword(int id, String password){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("password");
 
		Update update = new Update();
		update.set("password", password);
 
		mongoOperations.updateFirst(query, update, User.class);
	}*/
	
	@Override
	public void updateUsername(int id, String username){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("username");
 
		Update update = new Update();
		update.set("username", username);
 
		mongoOperations.updateFirst(query, update, User.class);
	}
	
	@Override
	public void updateEmail(int id, String email){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("email");
 
		Update update = new Update();
		update.set("email", email);
 
		mongoOperations.updateFirst(query, update, User.class);
	}
	
	@Override
	public void updateCash(int id, double cash){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("cash");
 
		Update update = new Update();
		update.set("cash", cash);
 
		mongoOperations.updateFirst(query, update, User.class);
	}
	
	@Override
	public List<Product> findHistory(int id){
		Query query = Query.query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, User.class).getHistoryList();
	}
	
	@Override
	public List<User> findAll() {
		return mongoTemplate.findAll(User.class);
	}

	@Override
	public User save(User user) {
		mongoTemplate.save(user);
		return user;
	}
	
	@Override
	public boolean updatePassword(int id_user, String password){
		User user = findById(id_user);
		user.setPass(password);
		mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(id_user)),Update.update("password",user.getPass()), User.class);
		return true;
		
	}
	
	@Override
	public User findByUserName(String username){
		Query query = Query.query(Criteria.where("username").is(username));
		return mongoTemplate.findOne(query, User.class);
	}
	
}
