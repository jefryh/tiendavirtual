package pe.edu.ucsp.cs.is2.repository;

import java.util.List;

import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.User;

public  interface UserRepository {
		
	User save(User user);
	User findByName(String name);
	User findById(int id);
	User findByUserName(String username);

	boolean updatePassword(int id_user, String password);
	void updateName(int id, String name);
	void updateLastName(int id, String lastname);
	void updateDate(int id, String date);
	//void updatePassword(int id, String password);
	void updateUsername(int id, String username);
	void updateEmail(int id, String email);
	void updateCash(int id, double cash);
	List<Product> findHistory(int id);
	List<User> findAll();
	
}
