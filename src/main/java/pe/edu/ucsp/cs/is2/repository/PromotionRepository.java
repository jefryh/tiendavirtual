package pe.edu.ucsp.cs.is2.repository;

import java.util.List;

import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.Promotion;

public interface PromotionRepository {
	Promotion save(Promotion promo,String name_product);
	Promotion findByName(String promotion);
	Promotion findById(int id);
}
