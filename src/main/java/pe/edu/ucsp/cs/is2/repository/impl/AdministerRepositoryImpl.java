package pe.edu.ucsp.cs.is2.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.cs.is2.domain.Administer;
import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.Promotion;
import pe.edu.ucsp.cs.is2.domain.Category;
import pe.edu.ucsp.cs.is2.domain.User;
import pe.edu.ucsp.cs.is2.repository.AdministerRepository;
import pe.edu.ucsp.cs.is2.repository.impl.CategoryRepositoryImpl;

@Repository
public class AdministerRepositoryImpl implements AdministerRepository{

	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;
	
	@Autowired(required = true)
	MongoOperations mongoOperations;
	
	@Override
	public Administer save(Administer administer){
		mongoTemplate.save(administer);
		return administer;
	}
	
	@Override
	public Administer findByName(String name){
		Query query = Query.query(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, Administer.class);
	}
	
	@Override
	public Administer findById(int id){
		Query query = Query.query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Administer.class);
	}
	
	@Override
	public void updateName(int id, String name){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("name");
 
		Update update = new Update();
		update.set("name", name);
 
		mongoOperations.updateFirst(query, update, Administer.class);
	}
	
	@Override
	public void updateLastName(int id, String lastname){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("last_name");
 
		Update update = new Update();
		update.set("last_name", lastname);
 
		mongoOperations.updateFirst(query, update, Administer.class);
	}
	
	@Override
	public void updateDate(int id, String date){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("birthdate");
 
		Update update = new Update();
		update.set("birthdate", date);
 
		mongoOperations.updateFirst(query, update, Administer.class);
	}
	
	/*public void updatePassword(int id, String password){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("password");
 
		Update update = new Update();
		update.set("password", password);
 
		mongoOperations.updateFirst(query, update, Administer.class);
	}*/
	
	@Override
	public void updateUsername(int id, String username){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("username");
 
		Update update = new Update();
		update.set("username", username);
 
		mongoOperations.updateFirst(query, update, Administer.class);
	}
	
	@Override
	public void updateEmail(int id, String email){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("email");
 
		Update update = new Update();
		update.set("email", email);
 
		mongoOperations.updateFirst(query, update, Administer.class);
	}
	
	@Override
	public List<Administer> findAll(){
		return mongoTemplate.findAll(Administer.class);
	}
	
	@Override
	public Product insertProduct(Product product){
		ProductRepositoryImpl p = new ProductRepositoryImpl();
		return p.save(product);
	}
	
	@Override
	public Category insertCategory(Category category, String name){
		CategoryRepositoryImpl p = new CategoryRepositoryImpl();
		return p.save(category,name);
	}
	
	@Override
	public Promotion insertPromotion(Promotion promotion, String name_product){
		PromotionRepositoryImpl p = new PromotionRepositoryImpl();
		return p.save(promotion,name_product);
	}
	
	@Override
	public User findUser(int id){
		UserRepositoryImpl u = new UserRepositoryImpl();
		return u.findById(id);
	}
	
	@Override
	public void updatePromotion(int id, float dis){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("discount");
 
		Update update = new Update();
		update.set("discount", dis);
 
		mongoOperations.updateFirst(query, update, Promotion.class);
	}
	
	@Override
	public void updatePromotion(int id, String end){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		query.fields().include("end_date");
 
		Update update = new Update();
		update.set("end_date", end);
 
		mongoOperations.updateFirst(query, update, Promotion.class);
	}
	
	@Override
	public int updateStock(String product_name, int stock){
		Query query = Query.query(Criteria.where("name").is(product_name));
		mongoTemplate.findOne(query, Product.class);
		
		Update update_p = Update.update("stock", stock);
		mongoTemplate.updateFirst(query, update_p, Product.class);
		return stock;
		
	}
	
	@Override
	public long getnumberProductCategory(int id){
		Query query = new Query();
		query.addCriteria(Criteria.where("category_id").is(id));
		
		return mongoTemplate.count(query, Product.class);
	}
	
	@Override
	public List<Product> getBestScoreProductCategory(int id){
		Query query = new Query();
		query.addCriteria(Criteria.where("category_id").is(id));
		query.with(new Sort(Sort.Direction.DESC,"score"));
		return mongoTemplate.find(query, Product.class);
	}
	
	@Override
	public List<Product> getTopProducts(){
		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC,"num_downloads"));
		return mongoTemplate.find(query, Product.class);
	}
	
	@Override
	public List<Product> getLessProducts(){
		Query query = new Query();
		query.with(new Sort(Sort.Direction.ASC,"num_downloads"));
		return mongoTemplate.find(query, Product.class);
	}
	
	@Override
	public List<Product> getProductsPrice(String maner){
		Query query = new Query();
		if(maner=="asc")
			query.with(new Sort(Sort.Direction.ASC,"priece"));
		if(maner=="desc")
			query.with(new Sort(Sort.Direction.DESC,"priece"));
		return mongoTemplate.find(query, Product.class);
	}
}
