package pe.edu.ucsp.cs.is2.repository;

import java.util.List;

import pe.edu.ucsp.cs.is2.domain.Category;

public interface CategoryRepository {

	Category save(Category category, String name);
	
	Category findById(int id);
	
	Category findByName(String name);

	List<Category> findAll();
	
	//void remove(Category Category);
}
