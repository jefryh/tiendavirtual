package pe.edu.ucsp.cs.is2.domain;

import java.util.List;

public class User extends Person {
	
	List<Product> list;
	List<Product> history_list;
	private double cash; 
	boolean is_active;
	
	protected User() {
		
	}
	public User(int id, String name,String last_name,String name_usuario,String password,String email,String birthdate){
		super(id,name,last_name,name_usuario,password,email, birthdate);
		is_active=true;
	}
	
	public double getCash(){
		return cash;
	}
	public void setCash(double cash){
		this.cash = cash;
	}
	public List<Product> getHistoryList(){
		return history_list;
	}
	public void unsubscribe(){
		is_active=false;
	}
}

	


