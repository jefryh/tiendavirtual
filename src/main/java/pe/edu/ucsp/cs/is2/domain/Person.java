package pe.edu.ucsp.cs.is2.domain;


public abstract class Person {

	protected int id;
	protected String name;
	protected String last_name;
	protected String username;
	protected String password;
	protected String email;
	protected String birthdate;

	public Person(){
		
	}
	
	public  Person(int id, String name,String last_name,String username,String password,String email,String birthdate){
		this.id=id;
		this.name = name;
		this.last_name = last_name;
		this.username = username;
		this.password = password;
		this.email = email;
		this.birthdate = birthdate;
	}

	public int getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public String getlast_name(){
		return last_name;
	}
	public String getUser(){
		return username;
	}
	public String getPass(){
		return password;
	}
	public String getEmail(){
		return email;
	}
	public String getFecha(){
		return birthdate;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setlast_name(String last_name){
		this.last_name = last_name;
	}
	public void setUser(String username){
		this.username = username;
	}
	public void setPass(String password){
		this.password = password;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public void setId(String birthdate){
		this.birthdate = birthdate;
	}
	public void loggen(){}
	public void edit_profile(){}
	public boolean find_user(){return false;};
	public boolean find_product(){return false;}
}
