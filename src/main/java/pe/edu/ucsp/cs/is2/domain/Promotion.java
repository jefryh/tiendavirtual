package pe.edu.ucsp.cs.is2.domain;

public class Promotion {
	private int id;
	private String ini_date;
	private String end_date;
	private float discount;
	
	public Promotion(int id, String ini_date, String end_date, float discount)
	{
		this.id = id;
		this.ini_date = ini_date;
		this.end_date =end_date;
		this.discount = discount;
	}
	
	public void set_data(int id, String ini_date, String end_date, float discount)
	{
		this.id = id;
		this.ini_date = ini_date;
		this.end_date =end_date;
		this.discount = discount;
	}


	
	public int getId(){
		return id;
	}
	
	public String getIniDate(){
		return ini_date;
	}
	
	public String getEndDate(){
		return end_date;
	}

	public float getDiscount(){
		return discount;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setIniDate(String ini_date){
		this.ini_date = ini_date;
	}
	
	public void setEndDate(String end_date){
		this.end_date = end_date;
	}

	public void setDiscount(float discount){
		this.discount = discount;
	}
}
