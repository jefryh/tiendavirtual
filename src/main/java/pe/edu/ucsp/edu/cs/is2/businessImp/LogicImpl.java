package pe.edu.ucsp.edu.cs.is2.businessImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.cs.is2.repository.ProductRepository;
import pe.edu.ucsp.cs.is2.repository.impl.ProductRepositoryImpl;
import pe.edu.ucsp.cs.is2.repository.impl.UserRepositoryImpl;
import pe.edu.ucsp.cs.is2.domain.Product;
import pe.edu.ucsp.cs.is2.domain.User;
import pe.edu.ucsp.edu.cs.is2.business.Logic;

@Repository
public class LogicImpl implements Logic {
	@Autowired(required = true)
	MongoDbFactory mongoDbFactory;

	@Autowired(required = true)
	MongoTemplate mongoTemplate;	
	
	@Override
	public boolean checkProduct(int idProduct){
		ProductRepository pro;
		
		boolean a = true;
		return true;
	}
	
	@Override
	public boolean verifyBuy(int id_user, int id_product){
		boolean product = false;
		boolean stock = false;
		boolean user = false;
		boolean cash = false;
		
		UserRepositoryImpl user_rep = new UserRepositoryImpl();
		User u = user_rep.findById(id_user); 
		if(u != null && u.getCash() != 0){
			user = true;
			cash = true;
		}
		ProductRepositoryImpl product_rep = new ProductRepositoryImpl();
		Product p = product_rep.findById(id_product);
		if(p != null && p.getStock() != 0){
			product = true;
			stock = true;
		}

		
		return product && stock && user && cash;
	}

}
