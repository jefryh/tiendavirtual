package pe.edu.ucsp.edu.cs.is2.business;

public interface Logic {

	public boolean checkProduct(int idProduct);
	public boolean verifyBuy(int id_user, int id_product);
}
